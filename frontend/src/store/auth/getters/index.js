import {Getterauth} from '../types'

export default {
    [Getterauth.getRole]: state => {
        return state.role;
    },
    [Getterauth.getTokenVerif]: state => {
        return state.tokenVerif;
    },
    
}