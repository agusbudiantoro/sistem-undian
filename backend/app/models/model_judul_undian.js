module.exports = (sequelize, Sequelize) => {
    const JudulUndian = sequelize.define("judul_undian", {
      judul: {
        type: Sequelize.STRING
      },
      company_name: {
        type: Sequelize.STRING
      },
      email_address: {
        type: Sequelize.STRING
      },
      code_number: {
        type: Sequelize.STRING
      },
      created_by: {
        type: Sequelize.INTEGER 
      },
    });
  
    return JudulUndian;
  };
  