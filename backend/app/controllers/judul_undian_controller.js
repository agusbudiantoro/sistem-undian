const db = require("../models");
const JudulUndian = db.judul_undian;
const Undian = db.undian;

exports.get = (req, res) => {
    JudulUndian.findAll()
        .then(data => {
            res.send(data);
        });
};

exports.getByCreated = (req, res) => {
    const id = req.params.id;
    JudulUndian.findAll({
        where: { created_by: id },
        include: Undian
    })
        .then(data => {
            res.send(data);
        });
};

exports.tambah = (req, res) => {
    console.log(req.body);
    console.log("sini");
    JudulUndian.create({
        judul: req.body.judul,
        userId: req.body.userId,
        company_name: req.body.company_name,
        email_address: req.body.email_address,
        code_number: req.body.code_number,
        created_by: req.body.created_by,
    }
    ).then(respons => {
        res.send("berhasil tambah judul undian reward")
    });
};

exports.edit = (req, res) => {
    const id = req.params.id;
    JudulUndian.update(req.body, {
        where: { id: id }
    })
        .then(isi => {
            res.send(isi);
        });
};

exports.delete = (req, res) => {
    const id = req.params.id;
    JudulUndian.destroy({ where: { id: id } })
        .then(res.send({ message: "Delete judul Undian berhasil" }))
};