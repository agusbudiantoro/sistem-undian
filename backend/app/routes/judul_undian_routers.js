const {authJwt} = require("../middleware");
const controller = require("../controllers/judul_undian_controller");

module.exports=function(app){
    app.use(function(req, res, next) {
        res.header(
          "Access-Control-Allow-Headers",
          "x-access-token, Origin, Content-Type, Accept"
        );
        next();
      });
    app.get("/api/judul_undian/:id",[authJwt.verifyToken],controller.getByCreated);
    app.post("/api/judul_undian",[authJwt.verifyToken],controller.tambah);
    app.put("/api/judul_undian/:id",[authJwt.verifyToken],controller.edit);
    app.delete("/api/judul_undian/:id",[authJwt.verifyToken],controller.delete);
}