const config = require("../config/config_db.js");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(
    config.DB,
    config.USER,
    config.PASSWORD,
    {
        host: config.HOST,
        dialect: config.dialect,
        operatorsAliases: false,

        pool: {
            max: config.pool.max,
            min: config.pool.min,
            acquire: config.pool.acquire,
            idle: config.pool.idle
        }
    }
);

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.user = require("./model_user.js")(sequelize, Sequelize);
db.role = require("./model_role.js")(sequelize, Sequelize);
db.peserta = require("./model_peserta.js")(sequelize, Sequelize);
db.master_reward = require("./model_master_reward.js")(sequelize, Sequelize);
db.undian = require("./model_undian.js")(sequelize, Sequelize);
db.judul_undian = require("./model_judul_undian")(sequelize, Sequelize);

db.role.belongsToMany(db.user, {
    through: "user_roles",
    foreignKey: "roleId",
    otherKey: "userId"
});
db.user.belongsToMany(db.role, {
    through: "user_roles",
    foreignKey: "userId",
    otherKey: "roleId"
});

db.peserta.hasMany(db.undian);
db.undian.belongsTo(db.peserta);

db.master_reward.hasMany(db.undian);
db.undian.belongsTo(db.master_reward);

db.judul_undian.hasMany(db.master_reward);
db.master_reward.belongsTo(db.judul_undian);

db.judul_undian.hasMany(db.undian);
db.undian.belongsTo(db.judul_undian);

db.judul_undian.hasMany(db.peserta);
db.peserta.belongsTo(db.judul_undian);

db.user.hasMany(db.judul_undian);
db.judul_undian.belongsTo(db.user);

db.master_reward.belongsToMany(db.user, {
    through: "reward_user",
    foreignKey: "rewardId",
    otherKey: "userId"
});


db.ROLES = ["user", "admin"];

// const Admin = sequelize.define('admin', { name: Sequelize.STRING }, { timestamps: false });
// const Task = sequelize.define('task', { name: Sequelize.STRING }, { timestamps: false });
// const Tool = sequelize.define('tool', {
//   name: Sequelize.STRING,
//   size: Sequelize.STRING
// }, { timestamps: false });
// Admin.hasMany(Task);
// Task.belongsTo(Admin);
// // Admin.hasMany(Tool, { as: 'Instruments' });
// db.admin = Admin;
// db.task = Task;
// db.tool = Tool;

module.exports = db;