const db = require("../models");
const Master_reward = db.master_reward;
const delete_file = require("../controllers/deleteFile");

exports.get=(req,res)=>{
    Master_reward.findAll()
    .then(data=>{
        res.send(data);
    });
};

exports.getByCreated=(req,res)=>{
    const id = req.params.id;
    Master_reward.findAll({
        where: {judulUndianId:id}
    })
    .then(data=>{
        res.send(data);
    });
};

exports.getById=function(id){
    return new Promise((resolve, reject)=>{
        Master_reward.findOne({
            where: {id:id}
        })
        .then(data=>{
            resolve(data);
        }).catch(err =>{
            reject(err);
        });
    });
    
};

exports.tambah=(req,res)=>{
    Master_reward.create({
        nama_hadiah:req.body.nama_hadiah,
        created_by:req.body.created_by,
        judulUndianId:req.body.judulUndianId,
        urutan_hadiah:req.body.urutan_hadiah,
        qty:req.body.qty,
        img:req.file.filename
    })
     .then(peserta=>{
        res.send(peserta);
     });
};

exports.edit=(req,res)=>{
    const id= req.params.id;
    if(req.body.img != null){
        delete_file.deleteGambar(req.body.img);
    }
    req.body.img = req.file.filename
    Master_reward.update(req.body,{
        where: {id:id}
    })
     .then(isi=>{
        res.send(isi);
     });
};

exports.minQty=function(data){
    data.qty=data.qty-1;
    
    return new Promise((resolve, reject)=>{
        Master_reward.update(data.dataValues,{
            where: {id:data.id}
        })
        .then(data=>{
            resolve(data);
        }).catch(err =>{
            reject(err);
        });
    });
    
};

exports.plusQty=function(data){
    data.qty=data.qty+1;
    
    return new Promise((resolve, reject)=>{
        Master_reward.update(data.dataValues,{
            where: {id:data.id}
        })
        .then(data=>{
            resolve(data);
        }).catch(err =>{
            reject(err);
        });
    });
    
};

exports.delete=(req,res)=>{
    const id= req.params.id;
    Master_reward.findOne({
        where: {id:id}
    })
    .then(data=>{
        if(data.img != null){
            delete_file.deleteGambar(data.img);
        }
    })
    Master_reward.destroy({where:{id:id}})
    .then(res.send({message:"Delete Master_reward berhasil"}))
};