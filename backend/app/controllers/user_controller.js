// const { where } = require("sequelize/types");
const db = require("../models");
const User= db.user;
const Role = db.role;
var dateTime = require('node-datetime');
var dt = dateTime.create();
var bcrypt = require("bcryptjs");
var fs = require('fs');
const exp = require("constants");

exports.allUser=(req,res)=>{
  User.findAll({
    include:{
      model: Role,
      through: { attributes: [] },
    }
  }).then(data=>{
    res.send(data);
  });
};

const base64_encode=(file)=>{
  // read binary data
  var bitmap = fs.readFileSync(file);
  // convert binary data to base64 encoded string
  return Buffer(bitmap).toString('base64');
}


exports.userbyid=(req,res)=>{
  User.findByPk(req.params.id)
  .then(data=>{
    
    res.send({
      username:data.dataValues.username,
      email:data.dataValues.email,
      foto:data.dataValues.foto
    });
  });
};

exports.hapususer=(req,res)=>{
  User.destroy({where:{id:req.params.id}}).then(res.send({message:"delete user berhasil"}))
};

exports.update=(req,res)=>{
  var tgl2 = dt.format('Y-m-d');
    var namafile="";
    if(req.file != null){
        namafile= tgl2+"-"+req.file.originalname;
        req.body.foto=namafile;
    }
  User.findByPk(req.params.id).then(data=>{
    if(req.body.password != ""){
      req.body.password=bcrypt.hashSync(req.body.password, 8);
   }else{req.body.password=data.dataValues.password}
    User.update(req.body,{
      where:{id:req.params.id}
      }).then(data=>{
        res.send(data);
      });
  });
  
};

exports.updaterole=(req,res)=>{
  User.findByPk(req.params.id).then(user=>{
      user.setRoles(req.body.roles).then(()=>{
        res.send({message:"Berhasil Update role"})
      });
  });
};

exports.allAccess = (req, res) => {
  res.status(200).send("Public Content.");
};

exports.userBoard = (req, res) => {
  res.status(200).send("User Content.");
};

exports.adminBoard = (req, res) => {
  res.status(200).send("Admin Content.");
};

exports.moderatorBoard = (req, res) => {
  res.status(200).send("Moderator Content.");
};
