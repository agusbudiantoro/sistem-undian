export const Actionauth = {
    Login : "Login",
    Logout: "Logout",
    parseJWT:'parseJWT',
    // api
    cekuser:'cekuser',
};

export const Mutationauth ={
    Login : "Login",
    request: "auth_request",
    success: "auth_success",
    error : "auth_error",
    Logout : "Logout",
    Islogin : "Islogin",
    Role:"Role",
    setTokenVerif:"setTokenVerif",

};

export const Getterauth = {
    getRole:"getRole",
    getTokenVerif:"getTokenVerif"
}