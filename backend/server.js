const express = require("express");
const cors = require("cors");

const app = express();


app.use(cors());

// parse requests of content-type - application/json
app.use(express.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

// database
const db = require("./app/models");
const Role = db.role;
const Noquot= db.ref_noquot;

db.sequelize.sync();

// routes
require('./app/routes/auth_routes')(app);
require('./app/routes/user_routes')(app);
require('./app/routes/role_routes')(app);
require('./app/routes/peserta_routes')(app);
require('./app/routes/master_reward_routes')(app);
require('./app/routes/undian_routes')(app);
require('./app/routes/judul_undian_routers')(app);

// set port, listen for requests
const PORT = process.env.PORT || 3001;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
