module.exports = (sequelize, Sequelize) => {
    const Master_reward = sequelize.define("master_reward", {
      nama_hadiah: {
        type: Sequelize.STRING
      },
      img: {
        type: Sequelize.TEXT
      },
      urutan_hadiah:{
        type: Sequelize.INTEGER
      },
      qty:{
        type: Sequelize.INTEGER
      },
      judulUndianId:{
        type: Sequelize.INTEGER
      },
      created_by:{
        type: Sequelize.INTEGER
      },
    });
    Master_reward.sync({alter: true});
    return Master_reward;
  };
  