const {authJwt} = require("../middleware");
const controller = require("../controllers/role_controller");

module.exports=function(app){
    app.use(function(req, res, next) {
        res.header(
          "Access-Control-Allow-Headers",
          "x-access-token, Origin, Content-Type, Accept"
        );
        next();
      });
    app.get("/api/role",[authJwt.verifyToken],controller.get);
    app.post("/api/role",[authJwt.verifyToken],controller.tambah);
    app.put("/api/role/:id",[authJwt.verifyToken],controller.edit);
    app.delete("/api/role/:id",[authJwt.verifyToken],controller.delete);
}