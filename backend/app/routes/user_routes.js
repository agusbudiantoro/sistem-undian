const { authJwt } = require("../middleware");
const controller = require("../controllers/user_controller");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.get("/api/user", controller.allUser);
  app.get("/api/cektoken",[authJwt.verifyToken],controller.allAccess);
  app.get(
    "/api/test/admin",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.adminBoard
  );
  app.get("/api/user/all",[authJwt.verifyToken,authJwt.isAdmin],controller.allUser);
  app.get("/api/user/:id",[authJwt.verifyToken,authJwt.isAdmin],controller.userbyid);
  app.put("/api/user/:id",[authJwt.verifyToken,authJwt.isAdmin],controller.update);
  app.put("/api/user/updaterole/:id",[authJwt.verifyToken,authJwt.isAdmin],controller.updaterole);
  app.delete("/api/user/hapus/:id",[authJwt.verifyToken,authJwt.isAdmin],controller.hapususer);
};
