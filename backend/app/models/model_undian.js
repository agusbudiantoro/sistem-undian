module.exports = (sequelize, Sequelize) => {
    const Undian = sequelize.define("undian", {
      id_peserta: {
        type: Sequelize.INTEGER
      },
      created_by: {
        type: Sequelize.INTEGER 
      },
      id_reward:{
        type: Sequelize.INTEGER
      },
      id_judul_undian:{
        type: Sequelize.INTEGER
      },
      alamat: {
        type: Sequelize.STRING
      },
    });
  
    return Undian;
  };
  