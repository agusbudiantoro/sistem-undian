const db = require("../models");
const Peserta = db.peserta;
const Undian = db.undian
const Master_reward = db.master_reward

exports.get=(req,res)=>{
    Peserta.findAll()
    .then(data=>{
        res.send(data);
    });
};

exports.getByCreated=(req,res)=>{
    const id = req.params.id;
    Peserta.findAll({
        where: {judulUndianId:id},
        include: Undian
    })
    .then(data=>{
        res.send(data);
    });
};

exports.tambah=(req,res)=>{
    Peserta.create({
        nomor_undian:req.body.nomor_undian,
        judulUndianId:req.body.judulUndianId,
        alamat:req.body.alamat,
        createdBy:req.body.createdBy,
        nama_peserta:req.body.nama_peserta,
    })
     .then(peserta=>{
        res.send(peserta);
     });
};

exports.edit=(req,res)=>{
    const id= req.params.id;
    Peserta.update(req.body,{
        where: {id:id}
    })
     .then(isi=>{
        res.send(isi);
     });
};

exports.delete=(req,res)=>{
    const id= req.params.id;
    Peserta.destroy({where:{id:id}})
    .then(res.send({message:"Delete peserta berhasil"}))
};