export const ActionUndian = {
    putUndian : "putUndian",
    postUndian: "postUndian",
    getAllUndian:'getAllUndian',
    delUndian:'delUndian',
    putJudulUndian : "putJudulUndian",
    postJudulUndian: "postJudulUndian",
    getAllJudulUndian:'getAllJudulUndian',
    delJudulUndian:'delJudulUndian',
};