const db = require("../models");
const controllerMasterReward = require("../controllers/master_reward_controller");
const Undian = db.undian;
const peserta = db.peserta;
const master_reward = db.master_reward;

exports.get=(req,res)=>{
    Undian.findAll()
    .then(data=>{
        res.send(data);
    });
};

exports.getByCreated=(req,res)=>{
    const id = req.params.id;
    Undian.findAll({
        where: {created_by:id},
        include: [peserta,master_reward]
        // include: {peserta: peserta, reward:master_reward},
    })
    .then(data=>{
        res.send(data);
    });
};

// exports.tambah=(req,res)=>{
//     console.log(req.body.length);
//     console.log("sini");
//     try {
//         for(var i=0;i<req.body.length;i++){
//             Undian.create({
//                 id_peserta:req.body[i].id_peserta,
//                 id_reward:req.body[i].id_reward,
//                 alamat:req.body[i].alamat,
//                 pesertumId: req.body[i].id_peserta,
//                 created_by:req.body[i].created_by,
//                 judulUndianId:req.body[i].judulUndianId,
//                 masterRewardId:req.body[i].masterRewardId
//             })
//             if(i == req.body.length-1){
//                 res.send("berhasil tambah peserta reward")
//             }
//         }
//     } catch (error) {
//         res.status.send(error);
//     }
// };

exports.tambah=async (req,res)=>{
    try {
            Undian.create({
                id_peserta:req.body.id_peserta,
                id_reward:req.body.id_reward,
                alamat:req.body.alamat,
                pesertumId: req.body.id_peserta,
                created_by:req.body.created_by,
                judulUndianId:req.body.judulUndianId,
                masterRewardId:req.body.id_reward
            }).then(async resp =>{
                let resMasterReward = await controllerMasterReward.getById(req.body.id_reward);
                if(resMasterReward.qty != null){
                    if(resMasterReward.qty > 0){
                        let resPut = await controllerMasterReward.minQty(resMasterReward);
                    }
                }
                await res.send("berhasil tambah peserta reward")
            })
                
    } catch (error) {
        res.status(403).send(error);
    }
};

exports.edit=(req,res)=>{
    const id= req.params.id;
    Undian.update(req.body,{
        where: {id:id}
    })
     .then(isi=>{
        res.send(isi);
     });
};

exports.delete=(req,res)=>{
    const id= req.params.id;
    const idreward= req.params.idreward;
    Undian.destroy({where:{id:id}})
    .then(
        
        async resp=>{
            console.log("sini");
            let resMasterReward = await controllerMasterReward.getById(idreward);
            if(resMasterReward.qty != null){
                let resPut = await controllerMasterReward.plusQty(resMasterReward);
            }
            res.send({message:"Delete Undian berhasil"});
        });
};