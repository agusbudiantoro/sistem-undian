import {Mutationauth} from '../types';

export default {
    [Mutationauth.request](state){
      state.status = 'loading'
    },
    [Mutationauth.setTokenVerif](state, stat){
      state.tokenVerif = stat;
    },
    [Mutationauth.success](state, token, user){
      state.status = 'success'
      state.token = token
      state.user = user
    },
    [Mutationauth.error](state){
      state.status = 'error'
    },
    [Mutationauth.Logout](state){
      state.status = ''
      state.token = ''
    },
  
    [Mutationauth.Islogin](state,islogin){
      state.islogin = islogin
    },
  
    [Mutationauth.Role](state,role){
      state.role=role
    },
  
  
  }