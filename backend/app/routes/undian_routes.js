const {authJwt} = require("../middleware");
const controller = require("../controllers/undian_controller");

module.exports=function(app){
    app.use(function(req, res, next) {
        res.header(
          "Access-Control-Allow-Headers",
          "x-access-token, Origin, Content-Type, Accept"
        );
        next();
      });
    app.get("/api/undian/:id",[authJwt.verifyToken],controller.getByCreated);
    app.post("/api/undian",[authJwt.verifyToken],controller.tambah);
    app.put("/api/undian/:id",[authJwt.verifyToken],controller.edit);
    app.delete("/api/undian/:id/:idreward",[authJwt.verifyToken],controller.delete);
}