module.exports = (sequelize, Sequelize) => {
    const Peserta = sequelize.define("peserta", {
      nomor_undian: {
        type: Sequelize.STRING
      },
      nama_peserta:{
        type: Sequelize.STRING
      },
      alamat: {
        type: Sequelize.STRING
      },
      createdBy: {
        type: Sequelize.INTEGER
      }
    });
  
    return Peserta;
  };
  